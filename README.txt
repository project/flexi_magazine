CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Flexi Magazine is a free mobile first, Bootstrap 3 based theme for Drupal 8,
which helps you to create a great looking news magazine site.

A flexible, theme with many regions and a responsive, mobile first drupal 8
theme for faster and easier web development.
It's Most flexible & user-friendly drupal theme.

REQUIREMENTS
------------

This theme requires the following libraries:
* Bootstrap CDN (Bootstrap, font-awesome)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/project/flexi_magazine
   for further information.

 * You may want to disable Toolbar module, since its output clashes with
   Administration Menu.


CONFIGURATION
-------------
 Bootstrap Layout
 It's Supported SASS and Font Awesome icons
 Multi-level drop-down menus
 Supported standard theme features: site logo, site name, site slogan,
 user pictures, favicon
 We have themed components like Forms Elements, Node Teaser, Comments, etc.
 Theme settings to further enhance for flexi magazine theme
 Upload the footer logo
 Add the social website links
 Upload the home page Banner image and description

MAINTAINERS
-----------
Sakthivel M (https://www.drupal.org/user/2947669)
Baskaran B (https://www.drupal.org/user/1470562)
